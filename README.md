Since opening our doors in 1995, Cleanville has become the premiere carpet cleaning company in Salt Lake City. We’re experts in carpet, rug and stone cleaning and care. We use the most effective equipment in the industry and Cleanville technicians are highly-trained professionals. Give us a call!

Address: 411 W Ironwood Dr, Salt Lake City, UT 84115, USA

Phone: 801-488-0088
